const fs = require('fs');
const csv = require('fast-csv');
var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "mydb"
});

fs.createReadStream('apps.csv')
  .pipe(csv())
  .on('data', function (data) {
    var app = data[0];
    var category = data[1];
    var rating = data[2];
    var reviews = data[3];
    var size = data[4];
    var installs = data[5];
    var version = data[6];
    console.log(app, category, rating, reviews, size, installs, version);
  })
  .on('end', function (data) {
    console.log('Read finished')
  });

var promisse_test = new Promise((resolve, reject) => {
  inserir(app, category, rating, reviews, size, installs, version);
  console.log("inserindo dados dahora");
})
  .then(function (resultado) {
    console.log('deu bom ai')
    return result
  })
  .catch(function (err) {
    console.log(err, 'deu erro ai')
  });

function inserir(data) {
  con.connect(function (err) {
    if (err) throw err;
    console.log("Connected!");
    var sql = "INSERT INTO tutorial (app, category, rating, reviews, size, installs, version) VALUES ?";
    con.query(sql, [data], function (err, result) {
      if (err) throw err;
      console.log("1 record inserted");
    });
  });
}

